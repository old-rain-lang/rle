use std::iter::{repeat, Repeat, Take};

/// A run of a given value stored with it's end
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct EndRun<V, L = usize> {
    /// The value the run is composed of
    pub value: V,
    /// The end of the run
    pub end: L,
}

impl<V> EndRun<V> {
    /// Iterate over this run given it's starting point
    pub fn iter_from(&self, start: usize) -> Take<Repeat<&V>> {
        let n = self.end.saturating_sub(start);
        repeat(&self.value).take(n)
    }
}

/// A run of a given value stored with it's length
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Run<V, L = usize> {
    /// The value the run is composed of
    pub value: V,
    /// The length of the run
    pub len: L,
}

impl<V> Run<V> {
    /// Iterate over this run
    pub fn iter(&self) -> Take<Repeat<&V>> {
        repeat(&self.value).take(self.len)
    }
    /// Get the length of this run
    pub fn len(&self) -> usize {
        self.len
    }
    /// Get whether this run is empty
    pub fn is_empty(&self) -> bool {
        self.len == 0
    }
}
